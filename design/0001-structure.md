# Structure

```mermaid
    graph TB
    subgraph User Script
        userScript 
        userScript --> Portfolio
    end
    userScript --> Strategy
    Strategy --> Data
    Strategy --> Account
    Account --> TradeList
    Strategy --> RiskMangement

```
- `Strategy` manages the open, modify and closing conditions
- `Data` manages the retrieval of price data and any other data specified
- `Account` manages the positions, open & closed trades and pending orders
- `TradeList` manages the retrieval of open, closed and pending orders
- `Strategy` acts a message passer. It calls upon `Data` and process the data and calls upon `Account` to handles the trades.

## Data
---
```mermaid
    graph TB
    Data --> CSV
    Data --> OandaData
    Data --> Quandl
    Data --> TDAmeritrade

```
- The different data types are `Fundmentals`, `Price Data`, `Indicators`
    - `Price Data` has 3 types: `OHLCV`, `OHLC`, `BidAsk`

## Account
---
```mermaid
    graph TB
        Account --> BackTest
        Account --> Oanda
        Account --> TDAmeritrade
        BackTest --> TradeList
        Oanda --> TradeList
        TDAmeritrade --> TradeList
```
- Each `Account` will have its own `TradeList` class to fetch for trades and orders
