# Backtest

This class handles all operations of running of backtest.  
Current implementation only allows hedging of position
- When a trade of opposite direction is open, it will not offset any open trade
- When a trades are closed, it will only close trades of the specific direction

## Helper Functions
---

### Open Trades
Handles the opening of trades and orders.
```py
    open_trade(instrument, order_type, trade_info)
```
Returns a trade identifier for the new open trade   

Arguments: 
- `instrument` : The instrument that the trade_info belongs to
- `order_type` : The type of entry. Only `market`, `stop` or `limit` are accepted
  - `order_type` of `stop` and `limit` would open a pending order
  - `order_type` of `market` would open a trade
- `trade_info` : A dictionary of the info of the trade. It must follow the following format:
```py
    {
        'timestamp': ...,
        'open_price': ...,
        'position_size': ..., # Positive position size indicates a long trade and a negative position size indicates a short trade. 
        'stop_loss_price': ...,
        'take_profit_price': ...
    }
```

### Modify Open Trades
Modification of open trades.
```py
    modify_trade(instrument, trade_id, stop_loss_price=None, take_profit_price=None)
```
Arguments: 
- `instrument` : The instrument that the trade_info belongs to
- `trade_id` : The unique identifier for an trade
- `stop_loss_price` : The new stop loss price 
- `take_profit_price` : The new take profit price
- `stop_loss_price` and `take_profit_price` cannot be empty at the same time

### Close Trades
Closes trades fully or partially. All instrument's open trade would be treated as one big trade.  
When given `position_size` > the sum of a direction's positions, a full close is assumed. 
```py
    close_trades(instrument, trade_info)
```
Arguments: 
- `instrument` : The instrument that the trade_info belongs to
- `trade_info` : A dictionary of the info of trades. It must follow the following format:
```py
    {
        'timestamp': ...,
        'exit_price': ...,
        'direction': ...,
        'position_size': ..., # If no `position_size` is given, the trade will be fully closed. 
    }
```

### Get Open Position Size
Return the sum of position size of trades open in the specified direction
```py
    get_open_position_size(instrument, direction)
```
- `instrument` : The instrument to retrieve the position size
- `direction` : The direction of the trade for the position size

### Manage Position
The method will check if any order needs to be executed, any stop loss and take profit is hit
```py
    manage_positions(instrument, price_data)
```
Arguments: 
- `instrument` : The instrument to check with
- `price_data` : Price data of an instrument. This `price_data` is the data stream from `Data` class.

TODO: To handle to situation where price hits stop loss and profit target in the same timestamp

### Calculate Profit OHLCV
This method will call `TradeList` to calculate the current period profit OHLCV
```py
    calculate_profit_ohlcv()
```