# Strategy

This class manages the open and closing conditions and execution of strategy.

Parts:
- Constructor
- Set Time Range
- Add Open Condition
- Add Close Condition
- Set Open Type
- Set Stop Loss
- Set Take Profit
- Set Risk
- Run Strategy

## Constructor
```py
    __init__(name, timeframe, data, account)
```
Arguments: 
- `name` : The name of the strategy
- `timeframe` : The timeframe to run the strategy
- `data` : The data object to retrieve data from
- `account` : The account object for execution of trades

## User functions
---

### Set time range 
Sets the time range to run the strategy on.  
*This function is only used for run backtest.*
```py
    set_time_range(start=None, end=None)
```
Arguments: 
- `start` :  Starting timestamp to run the backtest
- `end` : Ending timestamp to stop the backtest 

### Add Open Condition
Add a condition of to open a trade.
```py
    add_open_condition(direction, condition, how, 
                       look_back_type='single', 
                       look_back_period=1)
```
Arguments:
- `direction`: The direction of trade for the open condition. 
  Only `long` and `short` are valid.
- `condition` : The condition to open a position
- `how` : Indicates how a condition should be checked. 
  Only `switch` and `region` are valid.
  + `switch` : Only looks at the first instance on which the condition is true. 
    Subsequent signals will be ignored until the condition to false again.
  + `region`: Looks at all the instance where the condition is true.
- `look_back_type` : To look back a period or just a single period. 
  Only `range` and `single` are valid.
- `look_back_period` : How many period look back on for `range` type and 
  the period to look back on for `single`

When more than one condition is specified. All the conditions specified must be 
true for an action to be taken based on the conditions. 

### Add Close Condition
Add a condition of to close a trade.
```py
    add_close_condition(direction, condition, how, type, 
                        num_units=None, units_type=None, 
                        look_back_type='single', look_back_period=1)
```
Arguments:
- `direction`: The direction of trade for the open condition. 
  Only `long` and `short` are valid.
- `condition` : The condition to close a position
- `how` : Indicates how a condition should be checked. 
  Only `switch` and `region` are valid.
  + `switch` : Only looks at the first instance on which the condition is true. 
    Subsequent signals will be ignored until the condition to false again.
  + `region`: Looks at all the instance where the condition is true.
- `type` : The type of close when this condition is valid. 
  Only `full` and `partial` are valid
- `num_units` : (Only used when `type` is partial) Number of units to partially close.
- `look_back_type` : To look back a period or just a single period. 
  Only `range` and `single` are valid.
- `look_back_period` : How many period look back on for `range` type and 
  the period to look back on for `single`

When more than one condition is specified, all the conditions specified must be 
true for an action to be taken based on the conditions. 

#### Data Format
```py
    full_close_conditions = pd.DataFrame({
        'condition': ...
        'how': ...,
        'look_back_period': ...
    })

    partial_close_conditions = pd.DataFrame({
        'percentage': pd.DataFrame({
            '50' : pd.DataFrame({
                'condition': ...
                'how': ...,
                'look_back_period': ...
            }),
            ...
        }),
        'unit': pd.DataFrame({
            ...
        })
    })
```

### Set Open Type
Specifies how a trade should be open when all open conditions for the direction are met. 
```py
    set_open_type(direction, type, use, reference_points="", 
                  points_away="", look_back_type='single', 
                  look_back_period=1)
```
Arguments: 
- `direction`: The direction of trade for the open condition. 
  Only `long` and `short` are valid.
- `type` : The type of entry when the open conditions are true. 
  Only `market` and `order` are valid.
- `use` : (Should only use when `type` is `order`). 
  To state which reference point to use.
- `reference_points` : (Should only use when `type` is `order`). 
  The reference point for an order entry.
- `points_away` : (Should only use when `type` is `order`). 
  The number of points away from the reference point.
  + The format of the should be `sign multiplier points/indicator`. 
    The `SPACES` are need between each type e.g. `+ 2 ATR` or `- 1 1`.
- `look_back_type` : To look back a period or just a single period. 
  Only `range` and `single` are valid.
- `look_back_period` : How many period look back on for `range` type and 
  the period to look back on for `single`

### Set Stop Loss
Sets the initial stop loss when all conditions for the direction are met.
```py
    set_inital_stop_loss(direction, reference_points, use, 
                         points_away="", look_back_type='single', 
                         look_back_period=1)
```
Arguments:
- `direction`: The direction of trade for the open condition. 
  Only `long` and `short` are valid.
- `reference_points` : A list of points to set stop loss
- `use` : To state which reference point to use. 
  Only `lowest` and `highest` are valid. 
  + `lowest` : Use the lowest reference point
  + `highest` : Use the highest reference point
- `points_away` : number of points away from reference point
  + The format of the should be `sign multiplier points/indicator`. 
    The `SPACES` are need between each type e.g. `+ 2 ATR` or `- 1 1`.
- `look_back_type` : To look back a period or just a single period. 
  Only `range` and `single` are valid.
- `look_back_period` : How many period look back on for `range` type and 
  the period to look back on for `single`

### Set Take Profit
Sets the initial take profit when all conditions for the direction are met.
```py
    set_inital_take_profit(direction, reference_points, use, 
                           points_away="", look_back_type='single',
                           look_back_period=1)
```
Arguments:
- `direction`: The direction of trade for the open condition. 
  Only `long` and `short` are valid.
- `reference_points` : A list of points to set take profit
- `use` : To state which reference point to use. 
  Only `lowest` and `highest` are valid. 
  + `lowest` : Use the lowest reference point
  + `highest` : Use the highest reference point
- `points_away` : number of points away from reference point 
  + The format of the should be `sign multiplier points/indicator`. 
    The `SPACES` are need between each type e.g. `+ 2 ATR` or `- 1 1`.
- `look_back_type` : To look back a period or just a single period. 
  Only `range` and `single` are valid.
- `look_back_period` : How many period look back on for `range` type and 
  the period to look back on for `single`

### Set Risk
Set the risk type for the strategy
```py
    set_risk(type, value)
```
Arguments: 
- `type` : The type of risk. Only the type listed below are valid types
  + `fixedunits` : Using the same number of units for every trade
  + `variableunits` : TODO: ....
  + `fixedcash` : Using the same dollar amount of risk for every trade
  + `variablecash` : The dollar amount is dependent on the units specified 
    in relation to the account value
- `unit` : The corresponding value to the `type` specified

### Run Strategy
```py
    run()
```
This function will run on `backtest` or `live` depending on the account type 
specified in the constructor.

Procedure:  
In a Loop: 
1. Read the data
2. **(Only for Backtest)** Calls `account` to manage positions
3. Check to close
4. Check to modify
5. Check to open
6. **(Only for Backtest)** Calls `account` to calculate profit OHLCV 

### Getting the results 
```py
    .result
```
Returns a dataframe of all closed trades.

<!-- TODO: ADD HELPER FUNCTIONS IN -->
## Helper Function
---
### Condition parser
A parser function to parse string conditions.
```py
    __parse_condition(condition)
```
Returns `true` if the string condition is true 
