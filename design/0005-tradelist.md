# TradeList

## Constructor
```
    __init__(type, key=None, account=[])
```
Arguments: 
- `type` : The type of account. Only the following types are valid: 
  - `backtest`, `oanda`, `ib`, `tos`
- `key` : API key to connect to broker endpoint. It is required when `type` is not `backtest`. 
- `accounts` : It is required when `type` is `oanda`

## Helper Functions
--- 

### Get current trades
```py
    get_current_trades()
```
Returns a pandas dataframe of open trades.   

If `type` is `backtest`, this function will fetch the open trades from itself, else it will call the appropriate end points.

### Get historical trades
```py
    get_historical_trades()
```
Returns a pandas dataframe of historical trades.   

If `type` is `backtest`, this function will fetch the historical trades from itself, else it will call the appropriate end points.

### Get pending orders
```py
    get_pending_orders()
```
Returns a pandas dataframe of pending orders.   

If `type` is `backtest`, this function will fetch the pending orders from itself, else it will call the appropriate end points.

### Storing of trades
This function is mainly used for storing of BackTest trades. If `type` is not `backtest`, this function would raise an error.
```py
    store(type, instrument, trade_info)
```
Arguments:
- `type` : The type of trade. Only the following types are valid: 
  - `open`, `order`, `close`
- `instrument` : The instrument that the trade belongs to
- `trade_info` : The trade that needs to be store. Type: pandas dataframe

### Modification of open trades
This function is used to modify parameters of Backtest trades. E.g. modify of stop loss, take profit and position size.  If `type` is not `backtest`, this function would raise an error.
```py
    modify_trade(instrument, trade_id, type, parameters)
```
Arguments:
- `instrument` : The instrument that the trade_id belongs to
- `trade_id` : The unique identifier for the open trade
- `type`: The type of trade to modify. Only `current`, `historical` or `order` are valid.
- `parameters` : A list of parameters to modify

### Close of open trades or pending orders
Handles the removal of open trades
```py
    close(instrument, trade_id, type)
```
Arguments:
- `instrument` : The instrument that the trade_id belongs to
- `trade_id` : The unique identifier for the open trade
- `type` : The type to close. Only `order` or `open` are valid types.

### Calculate Profit OHLCV
```py
    calculate_profit_ohlcv()
```

Procedure: 
1. Calculate Profit OHLCV
2. Moves from open to current and close to historical

## How data is stored
---
```py
    'EURUSD' : pd.Dataframe,
    'USDJPY': pd.Dataframe,
```

Each Dataframe: 
- Open Trades
  - trade_id (index), timestamp, direction, position_size, entry_price, stop_loss_price, take_profit_price
- Closed Trades
  - trade_id (index), time_open, time_close, direction, position_size, entry_price, stop_loss_price, take_profit_price, exit_price
- Pending Orders
  - order_id (index), timestamp, direction, position_size, entry_price, stop_loss_price, take_profit_price