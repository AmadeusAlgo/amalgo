# Risk Management

- [Risk Management](#risk-management)
    - [Constructor](#constructor)
    - [Calculate position size](#calculate-position-size)

## Constructor
```python
    def __init__(type=type, unit=unit, percentage_risk=percentage_risk)
```

## Calculate position size
```python
    def calculate_position_size(entry_price, stop_loss_price, exchange_rate)
```