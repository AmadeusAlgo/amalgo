# Account

The Account class provides access to Amalgo's backtest engine for testing of strategies as well as to external brokers for live trading. 

## Constructor 
```py
    __init__(type, key=None, accounts=[])
```
Returns the corresponding Account class.
Arguments:
- `type` : Only the type below are valid
  - `backtest`, `oanda`, `ib`, `tos`
- `key` : API key to connect to broker endpoint. It is required when `type` is not `backtest`. 
- `accounts` : It is required when `type` is `oanda`. User will be allowed to add more than one account into a list and pass it as the argument.  

In the constructor, it will also create a `TradeList` object and pass in to the relevant account classes.
