from Amalgo import Strategy, Data, Account
from Amalgo.Indicators import SMA

# `Account` handles execution of trades
# `Data` handles data, includes csv and other data endpoints

#dataOanda = Data(type='oanda', key='some-key')
dataCSV = Data(type='csv') # To store csv files
dataCSV.add_instruments(['EURUSD', 'USDJPY'])
dataCSV.add_price_data('EURUSD', csv_file='EURUSD.csv') # Without specifying a type, type = ohlcv
dataCSV.add_price_Data('USDJPY', csv_file='USDJPY.csv', type='ohlc')
dataCSV.set_pips_type('USDJPY', pips_type=0.01)
dataCSV.addData('EURUSD', name='sentiments', csv_file='twitter.csv')
dataCSV.addData('EURUSD', name='MACross', csv_file='macross.csv') # Add indicator
dataCSV.set_past_period(2) # (Optional)
dataCSV['EURUSD'] # Prints EURUSD
#dataDuka = Data(type='duka') # To connect to duka endpoint
#dataTD = Data(type='TDAmer', key='some-key') # To connect to TDAmeritrade endpoint
#dataQuandl = Data(type='Quandl', key='some-key') # To connect to Quandl endpoint

# Assumes that we are only having 1 live Account and 1 backtest Account first

#acctOanda = Account(type='oanda', key='some-key', mainacct='000-000000-0-000') # To connect to oanda endpoint
acctBt = Account(type='backtest')
# TODO: Strategies dependent on currency to be implemented later
# Initial Balance is not considered just yet - trades are based either on risk level or number of units
# 

# mainacct is part of **kwargs, unique to oanda

strat = Strategy(name='MA Crossover', timeFrame='D', data=dataCSV, acct=acctBt)
#
# TODO: Get streaming csv file to work?
# src accepts Accounts only, csv files to be store in another Accounts class

# Assumes that we only use 1 TF

# strat.addIndicator('MACross', lambda x, y: SMA(x) - SMA(y), params=['fastMA', 'slowMA'])
# `on` parameter can be used to specify data other than the default (in this case, SMA always defaults to 'close')
# strat.addParameters(name='fastMA', period=20)
# strat.addParameters('fastMA', range(10,101,10)) # For optimisation backtesting
# strat.addParameters('slowMA', 50)
# strat.addParameters('slowMA', range(10,101,10)) # For optimisation backtesting

# TODO: Add actions though parameters; user shouldn't have direct access to the backtest

# Two types of `how` parameters - 'switch' and 'region'
# 'switch' only looks at the first instance of which the condition is true. Subsequent signals will be
# ignored until the condition changes to false again.
# 'region' looks at all the candles of which the signals are true.
# Strategies are recommended to have exactly 1 switch and as many regions as needed.

# Commands are parsed from strings, until we can find a more elegant solution.
# OHLCV, indicators and signals are assumed to use .shift(1) unless otherwise stated. This is 
# to avoid forward bias.

# strat.addIndicator('SMAfast', lambda x: SMA(x), params='fastMA')

# Type 1
strat.add_open_condition('long', 'MACross.high > 0', how='switch', type='full') # Example of fulling closing a trade
strat.add_open_condition('long', 'MACross.high > 0', how='switch', type='partial', num_units=50, units_type='percentage') # Example of partially closing of trade.
strat.add_open_condition('long', 'price.high > 100', how='switch', type='full') # Example of using price data in a condition
strat.add_open_condition('long', 'Donchian.high > 0', how='switch', type='full') # Example of choosing different price points to use for indicators
strat.add_open_condition('long', 'Donchian.high > 0', how='switch', type='full', look_back_type='range', look_back_period=2) # Example of looking back at a range of past period data set
strat.add_open_condition('long', 'Donchian.high > 0', how='switch', type='full', look_back_type='single', look_back_period=2) # Example of looking back at a specific past period, in this case, 2 periods back
strat.add_open_condition('long', 'RSI > 70', how='region') # Example of region
strat.set_open_type('long', type='order', reference_points=['price.high'], use='highest') # Example of a order entry
strat.set_open_type('long', type='market') # Example of market entry
strat.set_intial_stop_loss('long', reference_points=['price.low', 'price.close'], use='lowest', points_away='- 1 10', look_back_period=3)
strat.set_intial_take_profit('long', reference_points=['price.high', 'price.close'], use='highest', points_away='+ 1 ATR')
strat.add_close_condition('long', 'MACross < 0', how='switch') 

strat.add_open_condition('short', 'MACross < 0', how='switch') 
strat.add_close_condition('short', 'MACross > 0', how='switch') 

# Type 2
strat.positionSize(function) # TODO: Need to elaborate
# This is to accommodate to strats that needs to have a certain position size for each period

# `riskType` can be set to 'fixedUnits', 'fixedCash', 'varBal' and 'varUnits'
# TODO: Describe what it means by those riskTypes
strat.setRisk(riskType='fixedCash', value=3)
# fixedCash = x units per trade
# fixedCash = $x per trade (requires SL)
# varBal = x% per trade (requires SL)
# varUnits = $x per unit

# Errors won't show up until you run

strat.run(by='backtest')
# strat.run(by='live')

strat.result # Return the result of the strategy in a df
# strat.printResults() # For backtest only, in csv
#strat.printTearSheet() # Later
