Note: this repository is considered to be abandoned. Another iteration of the
Amalgo engine is in the works, but we plan to make it private first before and
present it once the engine is somewhat useable for public use.

# Amalgo

A Python 3 library used for building trading algorithms.

## Installing

1. Download this repository.
2. Move to the root repo repository folder
    ```
    cd path/to/project/folder
    ```   
3. Install with `pip`:

    ```
    pip install -e .
    ```

    The above command will also install the following prerequisites: `pandas`,
    `numpy`, `oandapyv20` (optional).

## For developers

The library `pytest` is required for testing purposes. To install, do
```
pip install pytest
```

## Documentation

Documentation can be found [here](design/).
