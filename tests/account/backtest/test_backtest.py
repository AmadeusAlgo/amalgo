import pytest
import pandas as pd
from pandas.testing import assert_frame_equal
import os

from Amalgo import Data
from Amalgo import Account
from Amalgo.account.backtest.InvalidTradeParameterError import InvalidTradeParameterError 

def test_openTrade_successful():
    EXPECTED_DIRECTION_LONG = "Long"
    EXPECTED_DIRECTION_SHORT = "Short"
    EXPECTED_STOP_LOSS_PRICE = 1.1
    EXPECTED_TAKE_PROFIT_PRICE = 3.1

    acctBt = create_backtest_csv_acct()

    # Position size > 0, no stop loss, no take profit price
    trade_id = acctBt.open_trade('abc', 0.1, 1.1)

    expected_trade = {
        'instrument': 'abc',
        'time': None,
        'direction': EXPECTED_DIRECTION_LONG,
        'position_size': abs(0.1),
        'entry_price': 1.1,
        'stop_loss_price': None,
        'take_profit_price': None
    }
    expected_trade_df = pd.DataFrame(expected_trade, index=[trade_id])
    expected_trade_series = expected_trade_df.loc[trade_id]
    trade = acctBt.open_trades().loc[trade_id]

    assert expected_trade_series.equals(trade)

    # Position size < 0, stop loss price, take profit price
    trade_id = acctBt.open_trade('abc', -0.1, 1.1, stop_loss_price=1.1, take_profit_price=3.1)

    expected_trade = {
        'instrument': 'abc',
        'time': None,
        'direction': EXPECTED_DIRECTION_SHORT,
        'position_size': abs(-0.1),
        'entry_price': 1.1,
        'stop_loss_price': EXPECTED_STOP_LOSS_PRICE,
        'take_profit_price': EXPECTED_TAKE_PROFIT_PRICE
    }
    expected_trade_df = pd.DataFrame(expected_trade, index=[trade_id])
    expected_trade_series = expected_trade_df.loc[trade_id]
    trade = acctBt.open_trades().loc[trade_id]

    assert expected_trade_series.equals(trade)

def test_openTradeInvalidPositionSize_raisesInvalidTradeParameterError():
    acctBt = create_backtest_csv_acct()

    # Position size == 0
    with pytest.raises(InvalidTradeParameterError): 
        trade_id = acctBt.open_trade('abc', 0, 1.1)

def test_modifyTrade_successful():
    EXPECTED_NEW_STOP_LOSS_PRICE = 0.8
    EXPECTED_NEW_TAKE_PROFIT_PRICE = 3.5

    acctBt = create_backtest_csv_acct()
    trade_id = open_template_trade(acctBt)

    expected_trade = {
        'instrument': 'abc',
        'time': None,
        'direction': 'Long',
        'position_size': abs(1),
        'entry_price': 1.1,
        'stop_loss_price': EXPECTED_NEW_STOP_LOSS_PRICE,
        'take_profit_price': EXPECTED_NEW_TAKE_PROFIT_PRICE
    }
    expected_trade_df = pd.DataFrame(expected_trade, index=[trade_id])
    expected_trade_series = expected_trade_df.loc[trade_id]

    acctBt.modify_trade(trade_id, stop_loss_price=0.8, take_profit_price=3.5)

    trade = acctBt.open_trades().loc[trade_id]

    assert expected_trade_series.equals(trade)

def test_closeTradeFull_successful():
    EXIT_PRICE = 1.5

    # No position size specified
    acctBt = create_backtest_csv_acct()
    trade_id = open_template_trade(acctBt)
    acctBt.close_trade(trade_id, 1.5)

    expected_closed_trade = {
        'instrument': 'abc',
        'time_open': None,
        'time_close': None,
        'direction': 'Long',
        'position_size': 1,
        'entry_price': 1.1,
        'stop_loss_price': 1.1,
        'take_profit_price': 3.1,
        'exit_price': EXIT_PRICE
    }
    expected_closed_trade_df = pd.DataFrame(expected_closed_trade, index=[trade_id])
    
    assert acctBt.open_trades().empty
    assert_frame_equal(expected_closed_trade_df, acctBt.closed_trades(), check_dtype=False)

    # Position size match existing position size
    acctBt = create_backtest_csv_acct()
    trade_id = open_template_trade(acctBt)
    acctBt.close_trade(trade_id, 1.5, position_size=1)

    expected_closed_trade_df = pd.DataFrame(expected_closed_trade, index=[trade_id])

    assert acctBt.open_trades().empty
    assert_frame_equal(expected_closed_trade_df, acctBt.closed_trades(), check_dtype=False)

def test_closeTradePartial_successful():
    EXIT_PRICE = 1.5
    EXIT_POSITION_SIZE = 0.5

    acctBt = create_backtest_csv_acct()
    trade_id = open_template_trade(acctBt)
    acctBt.close_trade(trade_id, 1.5, position_size=0.5)

    expected_open_trade = {
        'instrument': 'abc',
        'time': None,
        'direction': 'Long',
        'position_size': 0.5,
        'entry_price': 1.1,
        'stop_loss_price': 1.1,
        'take_profit_price': 3.1,
    }
    expected_open_trade_df = pd.DataFrame(expected_open_trade, index=[trade_id])

    expected_closed_trade = {
        'instrument': 'abc',
        'time_open': None,
        'time_close': None,
        'direction': 'Long',
        'position_size': 0.5,
        'entry_price': 1.1,
        'stop_loss_price': 1.1,
        'take_profit_price': 3.1,
        'exit_price': EXIT_PRICE
    }
    expected_closed_trade_df = pd.DataFrame(expected_closed_trade, index=[trade_id])

    open_trade = acctBt.open_trades()
    closed_trade = acctBt.closed_trades()

    assert_frame_equal(expected_open_trade_df, open_trade, check_dtype=False)
    assert_frame_equal(expected_closed_trade_df, closed_trade, check_dtype=False)  

def create_backtest_csv_acct():
    csv_filepath = os.path.join(os.path.dirname(__file__), '../../data/datasources/EURUSD.csv')

    dataCSV = Data(type='csv')
    dataCSV.addPriceData('EURUSD', csv_file=csv_filepath, type='OHLCV')

    acctBt = Account(type='Backtest', data=dataCSV)

    return acctBt

def open_template_trade(acctBt): 
    trade_id = acctBt.open_trade('abc', 1, 1.1, stop_loss_price=1.1, take_profit_price=3.1)

    return trade_id