import pytest
from Amalgo import Account
# Imported name here is long as you're not supposed to access DataCSV directly
from Amalgo.account.backtest.Backtest import Backtest
from Amalgo.account.AccountArgumentsError import AccountArgumentsError

def test_sameClass_returnTrue():
    # Type of Backtest
    DATA_SOURCE = "data_source"

    acctBt = Account(type='Backtest', data=DATA_SOURCE)
    assert isinstance(acctBt, Backtest)

def test_invalidAccountArgument_throwsAccountArgumentsError():
    # Type of Backtest with no data given
    with pytest.raises(AccountArgumentsError):
        acctBt = Account(type='Backtest')


test_invalidAccountArgument_throwsAccountArgumentsError()