import pytest
import pandas as pd
import os
from Amalgo import Data

expectedPriceData = {
    'close' : [1.1578, 1.1548, 1.1478, 1.1515, 1.1524],
    'high': [1.163, 1.1582, 1.1595, 1.1543, 1.155],
    'low': [1.1563, 1.1506, 1.1465, 1.1464, 1.1483],
    'open': [1.1609, 1.1578, 1.1548, 1.1478, 1.1513],
    'time': ['2018-10-01 14:00:00', '2018-10-02 14:00:00', '2018-10-03 14:00:00', '2018-10-04 14:00:00', '2018-10-05 14:00:00'],
    'volume': [24, 23, 22, 21, 20]
}

expectedPriceDataFrame = pd.DataFrame(expectedPriceData)
expectedPriceDataFrame.set_index('time', inplace=True)

expectedPriceOHLCData = {
    'close' : [1.1578, 1.1548, 1.1478, 1.1515, 1.1524],
    'high': [1.163, 1.1582, 1.1595, 1.1543, 1.155],
    'low': [1.1563, 1.1506, 1.1465, 1.1464, 1.1483],
    'open': [1.1609, 1.1578, 1.1548, 1.1478, 1.1513],
    'time': ['2018-10-01 14:00:00', '2018-10-02 14:00:00', '2018-10-03 14:00:00', '2018-10-04 14:00:00', '2018-10-05 14:00:00'],
}

expectedPriceOHLCDataFrame = pd.DataFrame(expectedPriceOHLCData)
expectedPriceOHLCDataFrame.set_index('time', inplace=True)

expectedData = {
    'close' : [1.1392, 1.1344, 1.1373],
    'high': [1.1477, 1.1387, 1.1418],
    'low': [1.1378, 1.1338, 1.1361],
    'open': [1.147, 1.1372, 1.1408],
    'time': ['2018-10-24 14:00:00', '2018-10-25 14:00:00', '2018-10-26 14:00:00'],
    'volume': [7, 3, 4]
}

expectedDataFrame = pd.DataFrame(expectedData)
expectedDataFrame.set_index('time', inplace=True)

def encapsulate_price_data(filename, type='ohlcv', remap={}):
    csv_filepath = os.path.join(os.path.dirname(__file__), '../datasources/', filename)

    dataCSV = Data(type='csv')
    dataCSV.add_instruments(['EURUSD', 'USDJPY'])
    dataCSV.add_price_data('EURUSD', csv_file=csv_filepath, type=type, remap=remap)
    df = dataCSV['EURUSD']['price']

    df.sort_index(axis=1, inplace=True)

    return df

def encapsulate_data(filename, data_name, remap={}):
    csv_filepath = os.path.join(os.path.dirname(__file__), '../datasources/', filename)

    dataCSV = Data(type='csv')
    dataCSV.add_instruments(['EURUSD', 'USDJPY'])
    dataCSV.add_data('EURUSD', name=data_name, csv_file=csv_filepath, remap=remap)
    df = dataCSV['EURUSD'][data_name]

    df.sort_index(axis=1, inplace=True)

    return df

def encapsulate_two_csv_data(filename1, filename2):
    first_csv_filepath = os.path.join(os.path.dirname(__file__), '../datasources/', filename1)
    second_csv_filepath = os.path.join(os.path.dirname(__file__), '../datasources/', filename2)

    dataCSV = Data(type='csv')
    dataCSV.add_instruments(['EURUSD', 'USDJPY'])
    dataCSV.add_data('EURUSD', name='data1', csv_file=first_csv_filepath,)
    dataCSV.add_data('EURUSD', name='data2', csv_file=second_csv_filepath)

def helper_add_price_test(filename, error, remap={}):
    csv_filepath = os.path.join(os.path.dirname(__file__), '../datasources/', filename)

    dataCSV = Data(type='csv')
    with pytest.raises(error):
        dataCSV.add_price_data('EURUSD', csv_file=csv_filepath)

def test_validPriceFile_returnTrue():
    # File with valid headers, sorted by time, and duplicated timings are dropped
    # Time with YYYYMMDD and sorted
    df = encapsulate_price_data('EURUSD_YYYYMMDD.csv')
    assert expectedPriceDataFrame.equals(df.head())

    # Time with DDMMYYYY
    # TODO: For future implementation

    # Duplicated Time
    df = encapsulate_price_data('Duplicated_Time_EURUSD.csv')
    assert expectedPriceDataFrame.equals(df.head())

    # Type OHLC
    df = encapsulate_price_data('EURUSD_OHLC.csv', type='ohlc')
    assert expectedPriceOHLCDataFrame.equals(df.head())

test_validPriceFile_returnTrue()

def test_invalidPriceFileHeaderWithRemap_returnTrue():
    df = encapsulate_price_data('Invalid_Headers_EURUSD.csv', remap = {'open_price' : 'open', 'date': 'time'})
    assert expectedPriceDataFrame.equals(df.head())

def test_invalidPriceFileHeaders_throwsError():
    helper_add_price_test('Invalid_Headers_EURUSD.csv', IndexError)

def test_validPriceFileHeadersWithRemap_throwsError():
    helper_add_price_test('EURUSD_YYYYMMDD.csv', KeyError, remap = {'open_price' : 'open', 'date': 'time'})

def test_validDataFile_returnsTrue():
    df = encapsulate_data('Other_data.csv', 'data1')
    assert expectedDataFrame.equals(df.head())

def test_invalidDataFileHeader_throwsError():
    with pytest.raises(IndexError):
        encapsulate_data('Invalid_Other_Data_Header.csv', 'data1')

def test_addTwoCSVFileTimingsDoesNotOverlap_throwsError():
    with pytest.raises(IndexError):
        encapsulate_two_csv_data('Other_Data.csv', 'Other_Data_NoOverlap.csv')
