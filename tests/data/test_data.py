import pytest
from Amalgo import Data
# Imported name here is long as you're not supposed to access DataCSV directly
from Amalgo.data.Services.CSV import DataCSV

def test_same_class():
    # DataCSV() should be the same as Data(type='csv')
    expectedDataCSV = DataCSV()
    dataCSV = Data(type='csv')
    assert expectedDataCSV == dataCSV
