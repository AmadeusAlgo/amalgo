class FunctionUsageError(Exception):
    """ Raise when related argument is not given for an Account class """
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message
