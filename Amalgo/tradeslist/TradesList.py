import pandas as pd

from .FunctionUsageError import FunctionUsageError

class TradesList:
    def __init__(self, type, key=None, accounts=''):
        self.__account_type = type.lower()
        self.__key = key
        self.__accounts = accounts

        self.__current_trades = {}
        self.__historial_trades = {}
        self.__pending_orders = {}

    def get_current_trades(self):
        if self.__account_type == 'backtest':
            return self.__current_trades

    def get_historical_trades(self):
        if self.__account_type == 'backtest':
            return self.__historial_trades

    def get_pending_orders(self): 
        if self.__account_type == 'backtest':
            return self.__pending_orders

    def store(self, type, instrument, trade_info):
        """
            (For backtest) To store the open, close trades and pending orders

            Arguments:
                type: The type of trade: open, close, order
                instrument: The instrument that the trade belongs to
                trade_info: The trade that need to be store. Type: Pandas DataFrame

            Raises: 
                FunctionUsageError: if account_type is not backtest
                ValueError: type is not a valid type
        """
        # TODO: To edit implementation for calculation of profit ohlcv
        if self.__account_type != 'backtest':
            raise FunctionUsageError('store() can only be used for Backtest')
        
        if type == "open":
            self.__backtest_store_open_trade(instrument, trade_info)
        elif type == "close":
            self.__backtest_store_close_trade(instrument, trade_info)
        elif type == "order":
            self.__backtest_store_pending_order(instrument, trade_info) 
        else: 
            raise ValueError('type is not valid. Only open, close or order are accepted values')

    def modify_trade(self, instrument, trade_id, type, parameters):
        """
            (For backtest) To modify parameters of a trade

            Arguments: 
                instrument: The instrument that the trade_id belong to
                trade_id: The unique identifier for a trade
                type: The type of trade to modify. Only current, historical or order are valid.
                parameters: The dictionary of parameters to modify

            Raises:
                KeyError: 
                    When the instrument does not exist
                    When the type specified is not valid
                FunctionUsageError: if type is not backtest
        """
        # TODO: Implement order in types
        if self.__account_type != 'backtest':
            raise FunctionUsageError('modify_trade() can only be used for Backtest')

        types = {
            'current': self.__current_trades,
            'historical': self.__historial_trades,
        }

        for key in parameters:
            types[type][instrument].at[trade_id, key] = parameters[key]

    def close(self, instrument, trade_id, type):
        """
            (For backtest) To close pending orders and current trades

            Arguments: 
                instrument: The instrument that the trade_id belongs to
                trade_id: The unique identifier for the open trade
                type: The type to close. Only order or open are valid types.
        """
        if self.__account_type != 'backtest':
            raise FunctionUsageError('close() can only be used for Backtest')

        if type == 'order':
            self.__pending_orders[instrument].drop(index=trade_id, inplace=True)
        elif type == 'open':
            self.__current_trades[instrument].drop(index=trade_id, inplace=True)
        else: 
            raise ValueError('type is not valid. Only order or open are accepted values')

    def calculate_profit_ohlcv(self):
        # TODO: Implementation
        pass

    def __backtest_store_open_trade(self, instrument, trade_info):
        if instrument not in self.__current_trades:
            # A new current trade for an instrument
            self.__current_trades[instrument] = pd.DataFrame(columns=[
                'trade_id', 'timestamp', 'direction', 'position_size',
                'entry_price', 'stop_loss_price', 'take_profit_price'
            ])
            self.__current_trades[instrument].set_index('trade_id', inplace=True)
        
        self.__current_trades[instrument] = self.__current_trades[instrument].append(trade_info)

    def __backtest_store_close_trade(self, instrument, trade_info):
        if instrument not in self.__historial_trades:
            # A new historical trade for an instrument
            self.__historial_trades[instrument] = pd.DataFrame(columns=[
                'trade_id', 'time_open', 'time_close',
                'direction', 'position_size',
                'entry_price', 'stop_loss_price', 'take_profit_price', 'exit_price'
            ])
            self.__historial_trades[instrument].set_index('trade_id', inplace=True)

        self.__historial_trades[instrument] = self.__historial_trades[instrument].append(trade_info)
    
    def __backtest_store_pending_order(self, instrument, trade_info):
        if instrument not in self.__pending_orders:
            # A new pending order for an instrument
            self.__pending_orders[instrument] = pd.DataFrame(columns=[
                'order_id', 'timestamp', 'direction', 'position_size',
                'entry_price', 'stop_loss_price', 'take_profit_price'
            ])
            self.__pending_orders[instrument].set_index('order_id', inplace=True)
        
        self.__pending_orders[instrument] = self.__pending_orders[instrument].append(trade_info)
