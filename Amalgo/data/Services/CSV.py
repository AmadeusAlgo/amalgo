import pandas as pd
import numpy as np

class DataCSV:
    def __init__(self): 
        self.__earliest_time = None
        self.__latest_time = None
        self.__instruments = {}
        self.__timestamps = []
        self.__past_period = 1
    
    def add_instruments(self, instruments):
        """
            Creates a container for the instrument's data

            Arguments: 
                instruments: A list of different instruments
        """
        
        for instrument in instruments: 
            self.__instruments[instrument] = {'price_type' : None, 'pips_move': 0.0001}
    
    def set_pips_move(self, instrument, pips_move):
        """
            Set the 1 pip move for an instrument

            Arguments:
                instrument: the instrument's name to map pips_move to
                pips_move: a float number of the move of 1 pip for an instrument
            
            Raises: 
                TypeError: instrument is not added before this function
        """
        self.__instruments[instrument]['pips_move'] = pips_move
    
    def add_price_data(self, instrument, csv_file, type='ohlcv', remap={}):
        """
            Reads the price data in the CSV file and adds it to the instrument's dictionary

            Arguments: 
                instrument: The instrument's name to map the price data to
                csv: The path to the csv data
                type: The type of data provided in the csv
                remap: A dictionary of headers that needs to be remaped

            Raises: 
                TypeError: instrument is not added before this function
                FileNotFoundError: An error occured while reading the csv_file
                IndexError: Invalid header/s given or when the timings provided in the csv file does not match
        """
        df = pd.read_csv(csv_file)
        self.__clean_csv(df, remap, type)
        self.__record_timings(df)

        self.__instruments[instrument]['price_type'] = type
        self.__instruments[instrument]['price'] = df

    def add_data(self, instrument, name, csv_file, remap={}):
        """
            Adds price data other than price data, indicator is added using this method

            Arguments: 
                instrument: The instrument's name to map the price data to
                csv: The path to the csv data
                name: The name of the data
                remap: A dictionary of headers that needs to be remaped 

            Raises: 
                TypeError: instrument is not added before this function
                FileNotFoundError: An error occured while reading the csv_file
                IndexError: Invalid header/s given
        """
        df = pd.read_csv(csv_file)
        self.__clean_csv(df, remap)
        self.__record_timings(df)

        self.__instruments[instrument][name] = df

    def set_past_period(self, period):
        """
            Sets number of past data to look at.

            Arguments:
                period: Number of period to look back at. 
        """
        self.__past_period = period     

    def generate_data(self):
        """
            Generates the valid price data and other data for use.

            Returns: 
                A generator of price data and other data.
        """        
        for timestamp in self.__timestamps: 
            for instrument in self.__instruments:
                current_data = []

                instrument_price_type = self.__instruments[instrument]['price_type']
                
                # No price data added
                if instrument_price_type is None:
                    continue

                current_index = self.__timestamps.index(timestamp)

                # If index < period, no point returning the price data, therefore continue
                if current_index < self.__past_period:
                    continue

                self.__handle_instrument_data_retrieval(instrument, timestamp, current_index, current_data)
            
                yield(current_data)   

    def __getitem__(self, key):
        return self.__instruments[key]

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __clean_csv(self, csv_df, remap, type=None):
        """
            Remaps headers, remove rows with duplicated time, sort by ascending

            Arguments: 
                csv_df: The csv's dataframe
                remap: To remap headers from CSV - named columns to standardised open, high, low, close(,volume)
                type: The type of csv data. When it is none
            
            Raises:
                ValueError: Type is not ohlcv or ohlc
                IndexError: When headers does not match expected headers
        """

        if remap: 
            csv_df.rename(columns=remap, inplace=True)

        if type is not None: 
            # This handles calls from add_price_data
            self.__validate_price_data_headers(csv_df, type)
        else:
            # This handles calls from add_data
            self.__validate_data_header(csv_df)

        csv_df['time'] = pd.to_datetime(csv_df.time)
        csv_df.drop_duplicates(subset=['time'], inplace=True)
        csv_df.set_index('time', inplace=True) 
        csv_df.sort_index(inplace=True)

    def __validate_price_data_headers(self, csv, type):
        types = {
            'ohlcv': ['time', 'open', 'high', 'low', 'close', 'volume'],
            'ohlc': ['time', 'open', 'high', 'low', 'close']
        }

        csv_headers = list(csv)
        expected_headers = types[type.lower()]

        if(set(csv_headers) != set(expected_headers)):
            raise IndexError('Headers does not match expected {}'.format(type))

    def __validate_data_header(self, csv):
        time = 'time'

        csv_headers = list(csv)

        if time not in csv_headers:
            raise IndexError('time is required in the data file.')        
    
    def __record_timings(self, df):
        """
            Stores the timestamps and records the earliest and lastest timestamps.

            Arguments: 
                df: The dataframe to read the time

            Raise: 
                IndexError: When the timings in the csv file do not overlap
        """
        earliest_time = df.iloc[0].name
        latest_time = df.iloc[-1].name

        self.__set_earliest_latest(earliest_time, latest_time)
        self.__set_timestamps(df)  
    
    def __set_earliest_latest(self, earliest_time, latest_time):
        """
            Set the earliest and the latest time to look at.
        """
        if (self.__earliest_time is None and self.__latest_time is None): 
            # Setting timings for the first time
            self.__earliest_time = earliest_time
            self.__latest_time = latest_time
        elif (self.__earliest_time > latest_time or self.__latest_time < earliest_time):
            raise IndexError("The time on the csv does not overlap with previous csv provided")
        else: 
            if (earliest_time > self.__earliest_time):
                self.__earliest_time = earliest_time

            if (latest_time < self.__latest_time):
                self.__latest_time = latest_time

    def __set_timestamps(self, df):
        """
            Sets and recaculate the timestamps array
        """
        if len(self.__timestamps) == 0:
            # Setting the timestamps for the first time
            self.__timestamps = df.index.tolist()
        else:
            # Slice the timestamps according to the latest and the earliest
            if self.__earliest_time in self.__timestamps:
                index = self.__timestamps.index(self.__earliest_time)
                self.__timestamps = self.__timestamps[index:]

            if self.__latest_time in self.__timestamps:
                index = self.__timestamps.index(self.__latest_time)
                self.__timestamps = self.__timestamps[:index + 1]
                        
    def __handle_instrument_data_retrieval(self, instrument, timestamp, current_index, current_data):
        """
            Handles the retrieval of price data
        """
        instrument_price_type = self.__instruments[instrument]['price_type']
        instrument_price_df = self.__instruments[instrument]['price']

        if instrument_price_type == 'ohlcv' or instrument_price_type == 'ohlc':
            current_open_price = instrument_price_df.loc[timestamp]['open']
            current_period = {
                'instrument': instrument,
                'timestamp': timestamp,
                'open': current_open_price
            }
            current_data.append(current_period)

        self.__handle_previous_periods_data(instrument, current_index, current_data)

    def __handle_previous_periods_data(self, instrument, current_index, current_data):
        """
            Retrieves the number of past data based on the past period specified by the user.
        """

        # Number of past data to look back           
        for i in range (0, self.__past_period):
            past_period_data = {}
            current_index -= 1

            past_timestamp = self.__timestamps[current_index]
            past_period_data['timestamp'] = past_timestamp

            for key in self.__instruments[instrument]:
                # Skip this variable for each instrument
                if key == 'price_type' or key == 'pips_move':
                    continue                

                instrument_data_df = self.__instruments[instrument][key]
                past_data = instrument_data_df.loc[past_timestamp].to_dict()   
                past_period_data[key] = past_data          
                
            current_data.append(past_period_data)
