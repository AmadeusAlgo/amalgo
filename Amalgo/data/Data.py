from .Services import *

"""
    Handle the different data sources

    Args:
        type: The type of data source

    Returns:
        The data source object

    Raises:
        TypeError: An invalid type is given
        KeyError: key is not given when type is not csv
"""

def data_wrapper(type='csv', key=None):
    types = {
        'csv': DataCSV()
    }

    return types[type.lower()]
