import pandas as pd
import numpy as np
import math

from Amalgo.account.backtest import Backtest
from Amalgo.utils import Utils
from Amalgo.exception.FunctionUsageError import FunctionUsageError

class Strategy: 
    def __init__(self, name, timeframe, data, account):
        """
        Arguments:
            name: The name of the strategy
            timeframe: The timeframe to run this strategy
            data: The data object to where the strategy will retrieve data from
            account: The account object to execute trade for the strategy
        """
        self.__name = name
        self.__timeframe = timeframe
        self.__data = data
        self.__account = account

        self.__start = None
        self.__end = None

        self.__long_open_how_switch = False
        self.__long_full_close_how_switch = False
        self.__long_partial_close_how_switches = {'percentage': {}, 'unit': {}}
        self.__long_open_conditions = pd.DataFrame()
        self.__long_full_close_conditions = pd.DataFrame()
        self.__long_partial_close_conditions = \
                self.__create_initial_partial_close_conditions_df()
        self.__long_SL_params = pd.DataFrame()
        self.__long_TP_params = pd.DataFrame()
        self.__long_open_type = "market"
        self.__long_open_order_params = pd.DataFrame()

        self.__short_open_how_switch = False
        self.__short_full_close_how_switch = False
        self.__short_partial_close_how_switches = {'percentage': {}, 'unit': {}}
        self.__short_open_conditions = pd.DataFrame()
        self.__short_full_close_conditions = pd.DataFrame()
        self.__short_partial_close_conditions = \
                self.__create_initial_partial_close_conditions_df()
        self.__short_SL_params = pd.DataFrame()
        self.__short_TP_params = pd.DataFrame()
        self.__short_open_type = "market"
        self.__short_open_order_params = pd.DataFrame()

        self.__risk = pd.DataFrame()

        # To store the result after running
        self.result = None 

    def set_time_range(self, start=None, end=None): 
        """
        Sets the time period to run this strategy on. 
        Only for Backtest.

        Arguments:
            start: 
                The starting timestamp to run the strategy
            end: 
                The ending timestamp to stop the stretegy

        Raises:
            FuntionUsageError: 
                When the account type specified is not Backtest
        """ 
        if type(self.__account) is not Backtest:
            raise FunctionUsageError('This function can only be used for backtest')

        self.__start = None
        self.__end = None

        if start is not None:
            self.__start = pd.Timestamp(start)
        if end is not None: 
            self.__end = pd.Timestamp(end)

    def add_open_condition(self, direction, condition, how, 
                           look_back_type='single', look_back_period=1):
        """
        Add a condition for opening of a trade 
        User are allowed to add more than one condition and 
        all conditions will need to be true for a trade to be opened.

        Arguments: 
            direction:
                The direction of trade to open. 
                Only 'long' and 'short' are valid directions.
            condition:
                The condition to open a trade
                It must be in the format of X EQUALITY Y. E.g. X > Y
            how:
                Indicates how a condition should be checked. 
                Only 'switch' and 'region' are valid. 
                switch:
                    Only looks at the first instance on which the condition 
                    is true. Subsequent signals will be ignored until the 
                    condition returns an opposite sign
                region:
                    Looks at all the instances where the condition is true
            look_back_period: 
                How many period look back on

        Raises:
            ValueError: if any of the arguments specified is not valid
        """
        direction = direction.lower()
        how = how.lower()
        look_back_type = look_back_type.lower()
        look_back_period = int(look_back_period)

        self.__validate_direction(direction)
        self.__validate_how(how)
        self.__validate_lookback(look_back_type)

        open_condition = {
            'condition': [condition],
            'how': [how],
            'look_back_type': look_back_type,
            'look_back_period': look_back_period
        }

        open_condition_df = pd.DataFrame(open_condition)

        if direction == "long":
            self.__long_open_conditions = self.__long_open_conditions.append(
                open_condition_df
            )
        elif direction == "short":
            self.__short_open_conditions = self.__short_open_conditions.append(
                open_condition_df
            )

    def add_close_condition(self, direction, condition, how, type, num_units=None, 
                            units_type=None, look_back_type='single', 
                            look_back_period=1):
        """
        Add a condition for closing of a trade 
        User are allowed to add more than one condition and 
        all conditions will need to be true for a trade to be closed.

        Arguments: 
            direction: 
                The direction of trade to close. 
                Only 'long' and 'short' are valid directions.
            condition: 
                The condition to close a trade
                It must falls in the format of X EQUALITY Y. E.g. X > Y
            how: 
                Indicates how a condition should be checked. 
                Only 'switch' and 'region' are valid. 
                switch: 
                    Only looks at the first instance on which the condition is true. 
                    Subsequent signals will be ignored until the condition returns 
                    an opposite sign
                region: Looks at all the instances where the condition is true
            type: 
                The type of close when this condition is valid. 
                Only `full` and `partial` are valid
            num_units: 
                Number of units to partially close.
                Only used when `type` is partial. 
            units_type: 
                Indicates what is the `units`. Only `percentage` and `unit` are valid.
                Only used when `type` is partial
            look_back_period: 
                How many period look back on

        Raises:
            ValueError: 
                if any of the arguments specified is not valid
        """        
        direction = direction.lower()
        how = how.lower()
        look_back_type = look_back_type.lower()
        look_back_period = int(look_back_period)
        type = type.lower()

        if num_units is not None:
            num_units = int(num_units)

        if units_type is not None:
            units_type = units_type.lower()

        self.__validate_how(how)
        self.__validate_lookback(look_back_type)
        self.__validate_close_type(type, num_units, units_type)

        close_condition = {
            'condition': [condition],
            'how': [how],
            'look_back_type': look_back_type,
            'look_back_period': look_back_period,
        }

        close_condition_df = pd.DataFrame(close_condition)

        close_type_arguments = {
            'type': type,
            'num_units': num_units,
            'units_type': units_type
        }

        if direction == 'long':
            if type == 'full':
                close_conditions, close_switches = self.__append_close_condition(
                    self.__long_full_close_conditions, 
                    self.__long_full_close_how_switch, 
                    close_condition_df, 
                    close_type_arguments
                )
                self.__long_full_close_conditions = close_conditions
            else:
                # To handle pass by value
                close_conditions, close_switches = self.__append_close_condition(
                    self.__long_partial_close_conditions, 
                    self.__long_partial_close_how_switches, 
                    close_condition_df, 
                    close_type_arguments
                )
                self.__long_partial_close_conditions = close_conditions
                self.__long_partial_close_how_switches = close_switches

        elif direction == "short":
            if type == 'full':
                close_conditions, close_switches = self.__append_close_condition(
                    self.__short_full_close_conditions, 
                    self.__short_full_close_how_switch,
                    close_condition_df, 
                    close_type_arguments
                )
                self.__short_full_close_conditions = close_condition
            else:
                close_conditions, close_switches = self.__append_close_condition(
                    self.__short_partial_close_conditions,
                    self.__short_partial_close_how_switches,
                    close_condition_df, 
                    close_type_arguments
                )
                self.__short_partial_close_conditions = close_conditions
                self.__short_partial_close_how_switches = close_switches
        else:
            raise ValueError("direction can only be long or short")

    def __append_close_condition(self, main_close_df, close_switches, 
                                 condition_df, close_type_arguments):
        if close_type_arguments['type'] == 'full':
            main_close_df = main_close_df.append(condition_df)
        else:
            # type=partial
            num_units = close_type_arguments['num_units']

            if close_type_arguments['units_type'] == 'percentage':
                if ((not main_close_df.loc['percentage']['data'].empty) and \
                   num_units in main_close_df.loc['percentage']['data'].index):
                    main_close_df.loc['percentage']['data'].loc[num_units]['data'] = \
                    main_close_df.loc['percentage']['data'].loc[num_units]['data'].append(
                        condition_df
                    )
                else: 
                    # New type of units
                    partial_percentage = {
                        'data': [condition_df]
                    }

                    partial_percentage_df = pd.DataFrame(
                        partial_percentage, index=[num_units]
                    )
                    # Create the new df for the new unit
                    main_close_df.loc['percentage']['data'] = \
                    main_close_df.loc['percentage']['data'].append(partial_percentage_df)
                    # Create the switch for the new unit
                    close_switches['percentage'][num_units] = False
            else: 
                # units_type='unit'
                if ((not main_close_df.loc['unit']['data'].empty) and \
                   num_units in main_close_df.loc['unit']['data'].index):
                    main_close_df.loc['unit']['data'].loc[num_units]['data'] = \
                    main_close_df.loc['unit']['data'].loc[num_units]['data'].append(
                            condition_df
                    )
                else: 
                    # New type of units
                    partial_unit = {
                        'data': [condition_df]
                    }

                    partial_unit_df = pd.DataFrame(partial_unit, index=[num_units])
                    # Create the new df for the new unit
                    main_close_df.loc['unit']['data'] = \
                    main_close_df.loc['unit']['data'].append(partial_unit_df)
                    close_switches['unit'][num_units] = False

        return (main_close_df, close_switches)
    
    def set_initial_stop_loss(self, direction, reference_points, use, 
                              points_away="", look_back_type='single', 
                              look_back_period=1):
        """
        Sets the initial stop loss when all conditions are for a direction are met.

        Arguments:
            direction: 
                The direction of trade to set the stop loss. 
                Only 'long' and 'short' are valid directions.
            reference_points: 
                A list of reference points to set the stop loss
            use: 
                To state which reference points to be used when the conditions are met. 
                Only 'lowest' and 'highest' are valid.
                lowest: 
                    Use the lowest reference point in the list of reference points
                highest: 
                    Use the highest reference point in the list of reference points
            points_away: 
                The number of points away from the selected reference points 
                to set the stop loss. 
                The sign infront of the points should be + or - to indicate whether 
                to add or subtract from the selected reference point.
            look_back_period: How many period look back on

        Raises: 
            ValueError: 
                if any of the arguments specified is not valid
        """

        direction = direction.lower()
        use = use.lower()
        look_back_type = look_back_type.lower()
        look_back_period = int(look_back_period)

        self.__validate_use(use)
        self.__validate_points_away(points_away)
        self.__validate_lookback(look_back_type)

        stop_loss_params = {
            'reference_points': [reference_points],
            'use': [use],
            'points_away': [points_away],
            'look_back_type': look_back_type,
            'look_back_period': look_back_period
        }
        
        stop_loss_params_df = pd.DataFrame(stop_loss_params)

        if direction == "long":
            self.__long_SL_params = stop_loss_params_df
        elif direction == "short":
            self.__short_SL_params = stop_loss_params_df
        else:
            raise ValueError("direction can only be long or short") 

    def set_initial_take_profit(self, direction, reference_points, use, 
                                points_away="", look_back_type='single',
                                look_back_period=1):
        """
        Sets the initial stop loss when all conditions are for a direction are met.

        Arguments:
            direction: 
                The direction of trade to set the stop loss. 
                Only 'long' and 'short' are valid directions.
            reference_points: 
                A list of reference points to set the take profit
            use: 
                To state which reference points to be used when the conditions are met. 
                Only 'lowest' and 'highest' are valid.
                lowest: 
                    Use the lowest reference point in the list of reference points
                highest: 
                    Use the highest reference point in the list of reference points
            points_away: 
                The number of points away from the selected reference points 
                to set the take profit. 
                The sign infront of the points should be + or - to indicate whether 
                to add or subtract from the selected reference point.
            look_back_period: 
                How many period look back on

        Raises: 
            ValueError: 
                if any of the arguments specified is not valid
        """
        direction = direction.lower()
        use = use.lower()
        look_back_type = look_back_type.lower()
        look_back_period = int(look_back_period)

        self.__validate_use(use)
        self.__validate_points_away(points_away)
        self.__validate_lookback(look_back_type)

        take_profit_params = {
            'reference_points': [reference_points],
            'use': [use],
            'points_away': points_away,
            'look_back_type': look_back_type,
            'look_back_period': look_back_period
        }
        
        take_profit_params_df = pd.DataFrame(take_profit_params)

        if direction == "long":
            self.__long_TP_params = take_profit_params_df
        elif direction == "short":
            self.__short_TP_params = take_profit_params_df
        else:
            raise ValueError("direction can only be long or short")

    def set_open_type(self, direction, type, use="", reference_points=[], points_away="", 
                      look_back_type='single', look_back_period=1):
        """
        Specifies how a position should be open when all open conditions are met. 

        Arguments:
            direction: 
                The direction of trade to set the stop loss. 
                Only 'long' and 'short' are valid directions.
            type: 
                The type of entry when the open conditions are met. 
                Only 'market' and 'order' are valid types.
            use: 
                To state which reference points to be used when the conditions are met. 
                Only 'lowest' and 'highest' are valid.
                lowest: 
                    Use the lowest reference point in the list of reference points
                highest: 
                    Use the highest reference point in the list of reference points
            reference_points: 
                The reference points to consider for an order entry
                Only used when type is order
            points_away: 
                The number of points away from the selected reference points 
                to set the order entry. 
                The sign infront of the points should be + or - to indicate whether 
                to add or subtract from the selected reference point.
            look_back_period: 
                How many period look back on

        Raises:
            ValueError: 
                if any of the arguments specified is not valid
        """
        direction = direction.lower()
        type = type.lower()
        look_back_type = look_back_type.lower()
        look_back_period = int(look_back_period)

        if type == "order":
            use = use.lower()

            self.__validate_use(use)
            self.__validate_points_away(points_away)

            order_params = {
                'reference_points': [reference_points],
                'use': [use],
                'points_away': [points_away],
                'look_back_type': look_back_type,
                'look_back_period': look_back_period
            }

            order_params_df = pd.DataFrame(order_params)

            if direction == "long":
                self.__long_open_type = "order"
                self.__long_open_order_params = order_params_df
            elif direction == "short":
                self.__short_open_type = "order"
                self.__short_open_order_params = order_params_df
            else:
                raise ValueError("direction can only be long or short")
        elif type == "market": 
            if direction == "long":
                self.__long_open_type = "market"
            elif direction == "short":
                self.__short_open_type = "market"
            else:
                raise ValueError("direction can only be long or short")
        else:
            raise ValueError("type can only be market or order")

    def set_risk(self, type, value):
        """
        Sets the amount of risk to take for each trade in the strategy

        Arguments:
            type: 
                The type of risk. Only the type listed below are valid types
                fixedunits: 
                    Using the same amount of units for every trade
                variableunits: 
                    TODO: ...
                fixedcash: 
                    Using the same dollar amount of risk for evert trade
                variablecash: 
                    Using the value given to calculated the dollar amount of risk 
                    to take for each trade. 
                    The dollar amount is dependent on the acccount value at each point 
                    in time

        Raises: 
            ValueError: 
                if type is not a valid risk type
        """
        type = type.lower() 

        self.__validate_risk_type(type)

        risk = {
            'type': [type],
            'value': [value]
        }

        self.__risk = pd.DataFrame(risk)

    def run (self):
        """
        Runs the strategy based on the parameters set. 
        
        Raises: 
            ValueError: 
                When risk is not set
                When there isn't sufficient data point to look back on 
        """
        print("Running Strategy...")

        if (self.__risk.empty):
            raise ValueError("Strategy needs a risk parameter to be set")

        for data in self.__data.generate_data():

            if type(self.__account) is Backtest:
                if self.__start is not None and data[0]['timestamp'] < self.__start:
                    continue

                if self.__end is not None and data[0]['timestamp'] > self.__end:
                    break

                print("Managing Backtest Positions")
                self.__account.manage_positions(data[0]['instrument'], data)

            self.__run_to_close(data)
            self.__run_to_open(data)

        self.result = self.__account.closed_trades()

    def __run_to_close(self, data):
        print("Checking to close positions")

        self.__run_to_full_close(data)
        self.__run_to_partial_close(data)

        print("")

    def __run_to_full_close(self, data):
        is_close_long = self.__is_condition_true(
            self.__long_full_close_conditions, 
            self.__long_full_close_how_switch, 
            data
        )
        is_close_short = self.__is_condition_true(
            self.__short_full_close_conditions, 
            self.__short_full_close_how_switch, 
            data
        )

        current_period_data = data[0]

        # Close Long Conditions    
        if is_close_long:
            # Set how to true so that subsequent similar switch condition 
            # will not be considered
            if self.__long_full_close_how_switch is False:
                self.__long_full_close_how_switch = True

            close_trade_info = {
                'timestamp': current_period_data['timestamp'],
                'exit_price': current_period_data['open'],
                'direction': 'long',
                'position_size': None
            }
            self.__account.close_trades(
                current_period_data['instrument'], 
                close_trade_info
            )    
        elif not is_close_long and self.__long_full_close_how_switch:
            # Reset the switch condition
            self.__long_full_close_how_switch = False

        # Close Short Conditions    
        if is_close_short:
            # Set how to true so that subsequent similar switch condition 
            # will not be considered
            if self.__short_full_close_how_switch is False:
                self.__short_full_close_how_switch = True
            close_trade_info = {
                'timestamp': current_period_data['timestamp'],
                'exit_price': current_period_data['open'],
                'direction': 'short',
                'position_size': None
            }
            self.__account.close_trades(
                current_period_data['instrument'], 
                close_trade_info
            )    
        elif not is_close_short and self.__short_full_close_how_switch:
            # Reset the switch condition
            self.__short_full_close_how_switch = False

    def __run_to_partial_close(self, data):
        current_period_data = data[0]

        # Partial Long Conditions
        for type_index, type_row in self.__long_partial_close_conditions.iterrows():
            for unit_index, unit_row in type_row['data'].iterrows():
                unit_switch = \
                self.__long_partial_close_how_switches[type_index][unit_index]
                # Using this instead of unit_row to get type of dataframe
                unit_condition = type_row['data'].loc[unit_index]['data']
                is_close_long = self.__is_condition_true(
                    unit_condition, unit_switch, data
                )

                if is_close_long:
                    # Set how to true so that subsequent similar switch condition 
                    # will not be considered
                    if self.__long_partial_close_how_switches[type_index][unit_index]\
                       is False:
                        self.__long_partial_close_how_switches[type_index][unit_index]\
                        = True

                    position_size = self.__calculate_partial_close_position_size(
                        current_period_data['instrument'], 
                        'long', type_index, unit_index
                    )

                    close_trade_info = {
                        'timestamp': current_period_data['timestamp'],
                        'exit_price': current_period_data['open'],
                        'direction': 'long',
                        'position_size': position_size
                    }
                    self.__account.close_trades(
                        current_period_data['instrument'], 
                        close_trade_info
                    )    
                elif not is_close_long and \
                     self.__long_partial_close_how_switches[type_index][unit_index]:
                    # Reset the switch condition
                    self.__long_partial_close_how_switches[type_index][unit_index]\
                    = False
        
        # Partial Short Conditions
        for type_index, type_row in self.__short_partial_close_conditions.iterrows():
            for unit_index, unit_row in type_row['data'].iterrows():
                unit_switch = \
                self.__short_partial_close_how_switches[type_index][unit_index]
                # Using this instead of unit_row to get type of dataframe
                unit_condition = type_row['data'].loc[unit_index]['data']
                is_close_short = self.__is_condition_true(
                    unit_condition, unit_switch, data
                )

                if is_close_short:
                    # Set how to true so that subsequent similar switch condition 
                    # will not be considered
                    if self.__short_partial_close_how_switches[type_index][unit_index]\
                       is False:
                        self.__short_partial_close_how_switches[type_index][unit_index]\
                        = True

                    position_size = self.__calculate_partial_close_position_size(
                        current_period_data['instrument'], 
                        'short', type_index, unit_index
                    )

                    close_trade_info = {
                        'timestamp': current_period_data['timestamp'],
                        'exit_price': current_period_data['open'],
                        'direction': 'short',
                        'position_size': position_size
                    }
                    self.__account.close_trades(
                        current_period_data['instrument'], 
                        close_trade_info
                    )    
                elif not is_close_short and\
                     self.__short_partial_close_how_switches[type_index][unit_index]:
                    # Reset the switch condition
                    self.__short_partial_close_how_switches[type_index][unit_index]\
                    = False
            
    def __calculate_partial_close_position_size(self, instrument, direction, 
                                                type, value):
        if type == 'unit':
            return value
        else: 
            # type='percentage'
            open_position = self.__account.get_open_position_size(instrument, direction)
            percentage_to_close = int(value) / 100
            positions_to_close = math.ceil(percentage_to_close * open_position)

            return positions_to_close

    def __run_to_open(self, data):
        print("Checking to open positions")

        current_period_data = data[0]
        past_periods_data = data[1:]

        is_open_long = self.__is_condition_true(
            self.__long_open_conditions, 
            self.__long_open_how_switch, 
            data
        )
        is_open_short = self.__is_condition_true(
            self.__short_open_conditions, 
            self.__short_open_how_switch, 
            data
        )

        # Long Open
        if is_open_long:
            stop_loss_price = None
            take_profit_price = None
            position_size = 0
            open_price = current_period_data['open']

            # Set how to true so that subsequent similar switch condition 
            # will not be considered
            if self.__long_open_how_switch is False:
                self.__long_open_how_switch = True

            if not self.__long_SL_params.empty:
                stop_loss_price = self.__calculate_stop_loss_price(
                    self.__long_SL_params, data
                )

            if not self.__long_TP_params.empty:
                take_profit_price = self.__calculate_take_profit_price(
                    self.__long_TP_params, data
                )

            if self.__long_open_type == "market":
                position_size = self.__calculate_open_position_size(
                    current_period_data['open'], stop_loss_price
                )
            else:
                # Order entry
                open_price = self.__calculate_order_entry_price(
                    self.__long_open_order_params, data
                )
                position_size = self.__calculate_open_position_size(
                    open_price, stop_loss_price
                )


            open_trade_info = {
                'timestamp': current_period_data['timestamp'],
                'open_price': open_price,
                'position_size': position_size,
                'stop_loss_price': stop_loss_price,
                'take_profit_price': take_profit_price
            }
            
            self.__account.open_trade(
                current_period_data['instrument'], 
                self.__long_open_type, 
                open_trade_info
            )    
        elif not is_open_long and self.__long_open_how_switch:
            # Reset the switch condition
            self.__long_open_how_switch = False

        # Short Open
        if is_open_short:
            stop_loss_price = None
            take_profit_price = None
            position_size = 0
            open_price = current_period_data['open']

            # Set how to true so that subsequent similar switch condition 
            # will not be considered
            if self.__short_open_how_switch is False:
                self.__short_open_how_switch = True

            if not self.__short_SL_params.empty:
                stop_loss_price = self.__calculate_stop_loss_price(
                    self.__short_SL_params, data
                )

            if not self.__short_TP_params.empty:
                take_profit_price = self.__calculate_take_profit_price(
                    self.__short_TP_params, data
                )

            if self.__short_open_type == "market":
                position_size = self.__calculate_open_position_size(
                    current_period_data['open'], stop_loss_price
                ) * -1
            else:
                # Order entry
                open_price = self.__calculate_order_entry_price(
                    self.__short_open_order_params, data
                )
                position_size = self.__calculate_open_position_size(
                    open_price, stop_loss_price
                ) * -1


            open_trade_info = {
                'timestamp': current_period_data['timestamp'],
                'open_price': open_price,
                'position_size': position_size,
                'stop_loss_price': stop_loss_price,
                'take_profit_price': take_profit_price
            }
            
            self.__account.open_trade(
                current_period_data['instrument'], 
                self.__short_open_type, 
                open_trade_info
            )    
        elif not is_open_short and self.__short_open_how_switch:
            # Reset the switch condition
            self.__short_open_how_switch = False
        
        print("")
    
    def __calculate_stop_loss_price(self, SL_params, data):
        print("Calculating Stop Loss")
        stop_loss_price = self.__calculate_price_point(SL_params, data)

        # TODO: Consider different way of handling negative value
        if (stop_loss_price < 0):
            raise ValueError("Stop loss price cannot hold a negative value")

        return stop_loss_price

    def __calculate_take_profit_price(self, TP_params, data):
        print("Calculating Take Profit")
        take_profit_price = self.__calculate_price_point(TP_params, data)

        if (take_profit_price < 0):
            raise ValueError("Take profit price cannot hold a negative value")

        return take_profit_price

    def __calculate_order_entry_price(self, order_params, data):
        print("Calculating Order Entry Price")
        order_entry_price =  self.__calculate_price_point(order_params, data)

        if (order_entry_price < 0):
            raise ValueError("Order entry price cannot hold a negative value")

        return order_entry_price
    
    def __calculate_price_point(self, params, data):
        price = None

        reference_points = params.reference_points[0]
        use = params.use[0]
        points_away = params.points_away[0]
        look_back_period = params.look_back_period[0]
        
        for i in range(1, look_back_period+1):
            # To get the lowest/highest price within the reference points and 
            # within the look back period
            temp_price = self.__get_reference_price(reference_points, use, data[i])

            if price is None: 
                price = temp_price
            elif use == "highest" and price < temp_price:
                price = temp_price
            elif use == "lowest" and price > temp_price:
                price = temp_price

        if points_away != "":
            price = self.__append_points_away(price, points_away, data)

        return price 

    def __get_reference_price(self, reference_points, use, data):
        prices = []

        for reference_point in reference_points:
            price = self.__parse_condition(reference_point, data)
            prices.append(price)
        
        sort_descending = False
        if use == "highest":
            sort_descending = True
        
        prices.sort(reverse=sort_descending)

        return prices[0]

    def __append_points_away(self, price, points_away, data):        
        points_away_arguments = points_away.split(" ")
        sign = points_away_arguments[0]
        multipler = float(points_away_arguments[1])
        points = points_away_arguments[2]

        # It is a points_away with not a price reference. E.g. ATR,
        if not points.isdigit():
            points = self.__parse_condition(points, data)
        else:
            points_base = self.__data[data[0]['instrument']]['pips_move']
            points = int(points) * points_base

        if sign == '+':
            price += multipler * points
        else: 
            price -= multipler * points

        return price
    
    def __calculate_open_position_size(self, open_price, stop_loss_price):
        """
            Returns the position size in number units
        """
        if (self.__risk.type[0] == "fixedcash" and stop_loss_price is None):
            raise KeyError("risk type of fixedcash needs to have a stop loss price set.")

        # Future: Consider Exchange Rate & Forex Lot Size

        position_size = {
            'fixedunits': lambda: float(self.__risk.value[0]),
            'fixedcash': lambda: math.floor(float(self.__risk.value[0]) /\
                                 abs(open_price - stop_loss_price))
        }

        return position_size[self.__risk.type[0]]()

    def __is_condition_true(self, conditions, how, data):
        are_conditions_true = True

        # If there are no conditions
        if conditions.empty:
            return False

        for index, row in conditions.iterrows():
            condition = row['condition']
            condition_how = row['how']
            look_back_type = row['look_back_type']
            look_back_period = row['look_back_period']

            # If look_back_period > the range set in the csv
            if (look_back_period > len(data) - 1):
                raise ValueError(
                    'Insufficient data points to look back on. '
                    'look_back_period is set to {}, and data range is only {}'.format(
                        look_back_period, len(data)-1
                    )
                )

            if look_back_type == 'range':
                is_condition_true = self.__is_range_condition_satisfied(
                    data, condition, look_back_period
                )
            else: 
                is_condition_true = self.__is_single_condition_satisfied(
                    data, condition, look_back_period
                )

            # Handling of when how is switch
            if is_condition_true:
                # Even when the condition is true, 
                if condition_how == "switch" and how is True:
                    return False
            else:
                return False

        return are_conditions_true

    def __is_range_condition_satisfied(self, data, condition, look_back_period):
        # every condition needs to be true for each look back period 
        # in order for the overall condition to be true
        for i in range(1, look_back_period+1):              
            split_condition = condition.strip().split()
            left_condition = self.__parse_condition(split_condition[0], data[i])
            operator = split_condition[1]
            right_condition = self.__parse_condition(split_condition[2], data[i])

            # TODO: Handle the adding of conditions (high + low > 0)

            operators = {
                '>' : lambda: left_condition > right_condition,
                '<' : lambda: left_condition < right_condition,
                '>=': lambda: left_condition >= right_condition,
                '<=' : lambda: left_condition <= right_condition,
                '=' : lambda: left_condition == right_condition,
                '==' : lambda: left_condition == right_condition,
            }

            is_condition_true = operators[operator]()

            if not is_condition_true:
                return False
        
        return True

    def __is_single_condition_satisfied(self, data, condition, look_back_period):
        split_condition = condition.strip().split()
        left_condition = self.__parse_condition(
            split_condition[0], data[look_back_period]
        )
        operator = split_condition[1]
        right_condition = self.__parse_condition(
            split_condition[2], data[look_back_period]
        )

        # TODO: Handle the adding of conditions (high + low > 0)

        operators = {
            '>' : lambda: left_condition > right_condition,
            '<' : lambda: left_condition < right_condition,
            '>=': lambda: left_condition >= right_condition,
            '<=' : lambda: left_condition <= right_condition,
            '=' : lambda: left_condition == right_condition,
            '==' : lambda: left_condition == right_condition,
        }

        is_condition_true = operators[operator]()

        return is_condition_true

    def __parse_condition(self, condition, data):
        parsed_condition = data

        if Utils.is_float(condition):
            parsed_condition = condition
        else: 
            condition_list = condition.strip().split('.')
            for individual_condition in condition_list:
                parsed_condition = parsed_condition[individual_condition]

        return float(parsed_condition)

    def __create_initial_partial_close_conditions_df(self):        
        data_df = pd.DataFrame(columns=['num_units', 'data'])
        data_df.set_index('num_units', inplace=True) 

        partial_close = {
            'type': ['percentage', 'unit'],
            'data': [data_df, data_df]
        }

        partial_close_df = pd.DataFrame(partial_close)
        partial_close_df.set_index(['type'], inplace=True)

        return partial_close_df
    
    def __validate_direction(self, direction):
        VALID_DIRECTION_CONDITION = ['long', 'short']

        if (direction.lower() not in VALID_DIRECTION_CONDITION):
            raise ValueError(
                "Invalid direction condition. direction can only take " +\
                ", ".join([str(i) for i in VALID_DIRECTION_CONDITION])
            )

    def __validate_how(self, how):
        VALID_HOW_CONDITION = ['switch', 'region']

        if (how.lower() not in VALID_HOW_CONDITION):
            raise ValueError(
                "Invalid how condition. how can only take " +\
                ", ".join([str(i) for i in VALID_HOW_CONDITION])
            )

    def __validate_lookback(self, lookback_type):
        VALID_LOOKBACK_CONDITION = ['single', 'range']

        if (lookback_type.lower() not in VALID_LOOKBACK_CONDITION):
            raise ValueError(
                "Invalid lookback type. lookback_type can only take " +\
                ", ".join([str(i) for i in VALID_LOOKBACK_CONDITION])
            )

    def __validate_close_type(self, close_type, num_units, units_type):
        VALID_CLOSE_TYPE = ['full', 'partial']
        VALID_UNITS_TYPE = ['percentage', 'unit']

        if close_type.lower() not in VALID_CLOSE_TYPE:
            raise ValueError(
                "Invalid close type condition. type can only take " +\
                ", ".join([str(i) for i in VALID_CLOSE_TYPE])
            )

        if close_type.lower() == 'partial':
            if num_units is None or units_type is None: 
                raise ValueError(
                    "num_units and units_type needs to be specified "
                    "if type is partial"
                )

            if units_type not in VALID_UNITS_TYPE:
                raise ValueError(
                    "Invalid units type condition. units_type can only take " +\
                    ", ".join([str(i) for i in VALID_UNITS_TYPE])
                )

    def __validate_use(self, use):
        VALID_USE = ["lowest", "highest"]

        if(use.lower() not in VALID_USE):
            raise ValueError(
                "Invalid use type. use can only take " +\
                ", ".join([str(i) for i in VALID_USE])
            )

    def __validate_risk_type(self, type):
        # To include 'variableunits' and 'variablecash' to type in the future
        VALID_RISK_TYPE = ['fixedunits', 'fixedcash']

        if(type not in VALID_RISK_TYPE):
            raise ValueError(
                "Invalid how type. type can only take " +\
                ", ".join([str(i) for i in VALID_RISK_TYPE])
            )

    def __validate_points_away(self, points_away):
        points_away_arguments = points_away.split(" ")

        if len(points_away_arguments) != 3:
            raise ValueError(
                "Invalid points_away. points_away needs to follow this format: "
                "sign multipler price/indicator"
            )
        
        if not ('+' == points_away_arguments[0]  or '-' == points_away_arguments[0]):
            raise ValueError(
                "Invalid points_away. points_away needs to have either "
                "- or + at the start"
            )
