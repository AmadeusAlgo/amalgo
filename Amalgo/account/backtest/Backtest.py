import pandas as pd
import uuid

from Amalgo.data.Services import DataCSV


class Backtest(object):
    def __init__(self, tradeslist):
        self.__tradeslist = tradeslist

    def open_trade(self, instrument, order_type, trade_info):
        """
            Opening of new trades

            Arguments: 
                instrument: The instrument that the trade_info belongs to
                order_type: The type of entry. Only 'market', 'stop' or 'limit' are accepted
                trade_info: A dictionary of info of the open trade.
                    position_size:  Positive position size indicates a long trade.
                                    Negative position size indicates a short trade
            
            Returns: 
                A trade indentifier for the new open trade.

            Raises: 
                KeyError: 
                    When one of required trade_info parameter is not given
                ValueError: 
                    The order_type is not valid
                    When the position_size is 0
        """
        trade_id = uuid.uuid4()
        df = self.__create_open_trade_df(trade_id, trade_info)

        # Market Entry 
        if order_type == 'market':
            self.__market_entry(instrument, df)
        elif order_type == 'stop' or 'limit':
            self.__order_entry(instrument, df)
        else: 
            raise ValueError("order_type is not valid. Only market, stop or limit are accepted values")

        return trade_id          

    def modify_trade(self, instrument, trade_id, stop_loss_price=None, take_profit_price=None):
        """
            Modification of stop loss and take profit price of open trades

            Arguments: 
                trade_id: The unique indentifier for an open trade
                stop_loss_price: (Optional) The new stop loss price
                take_profit_price: (Optional) The new take profit price

            Raises: 
                KeyError: 
                    When the trade_id is not an open trade
                    When the instrument does not exist
                ValueError: 
                    When stop_loss_price and take_profit_price is None at the same time
        """
        if stop_loss_price is None and take_profit_price is None:
            raise ValueError("stop loss and take profit price cannot be None at the same time")

        params_to_modify = {}

        if stop_loss_price is not None: 
            params_to_modify['stop_loss_price'] = stop_loss_price

        if take_profit_price is not None: 
            params_to_modify['take_profit_price'] = take_profit_price

        self.__tradeslist.modify_trade(instrument, trade_id, type='current', parameters=params_to_modify)

    def close_trades(self, instrument, trade_info):
        """
            Fully or partially close open trades

            Arguments:
                instrument: The instrument that the trade_info belongs to
                trade_info: A dictionary of the info of the trade. 

            Raises:
                KeyError: 
                    When the instrument does not exist
                    When one of required trade_info parameter is not given                 
        """

        # All instrument's trade a treated as one big trade. If no position size is provided, it will close all trade.

        direction_to_close = trade_info['direction']

        open_trades = self.__tradeslist.get_current_trades()
        
        open_trades = open_trades[instrument]
        total_open_size = open_trades.loc[open_trades['direction'] == direction_to_close, 'position_size'].sum()

        size_to_close = trade_info['position_size']

        if size_to_close is None or size_to_close == total_open_size:
            self.__close_trades_full(instrument, trade_info)
        elif size_to_close < total_open_size:
            self.__close_trades_partial(instrument, trade_info)
        else: 
            # If given position_size, will just close the trade full
            self.__close_trades_full(instrument, trade_info)

    def get_open_position_size(self, instrument, direction):
        """
            Returns the sum of position size of trades open in the specified direction

            Arguments: 
                direction: The direction of the trade for the sum of position size

            Raises: 
                KeyError: 
                    When the direction is invalid
                    When the instrument does not exist
        """
        open_trades = self.__tradeslist.get_current_trades()
        open_trades = open_trades[instrument]
        return open_trades.loc[open_trades['direction'] == direction, 'position_size'].sum()
    
    def manage_positions(self, instrument, price_data):
        """
            Manages execution of stop loss, take profit and orders

            Arguments:
                instrument: The instrument to check for
                price_data: It is the data stream provided by the Data class. It is the price data at a point in time.
        """
        self.__manage_open_trades(instrument, price_data)
        self.__manage_pending_orders(instrument, price_data)
    
    def calculate_profit_ohlcv(self):
        self.__tradeslist.calculate_profit_ohlcv()
    
    def __market_entry(self, instrument, new_trade_df):
        self.__tradeslist.store('open', instrument, new_trade_df)

    def __order_entry(self, instrument, new_order_df):
        self.__tradeslist.store('order', instrument, new_order_df)

    def __close_trades_full(self, instrument, trade_info):
        open_trades = self.__tradeslist.get_current_trades()[instrument]

        for index, row in open_trades.iterrows():
            open_trade = open_trades.loc[index]
            open_trade_direction = open_trade.direction

            old_entry = instrument in self.__tradeslist.get_historical_trades() and index in self.__tradeslist.get_historical_trades()[instrument].index

            if open_trade_direction == trade_info['direction']: 
                self.__tradeslist.close(instrument, index, type='open')

                if old_entry:
                    # Trade was previously partially closed
                    position_closed = self.__tradeslist.get_historical_trades()[instrument].loc[index].position_size
                    params_to_modify = {
                        'position_size': position_closed + open_trade.position_size 
                    }
                    self.__tradeslist.modify_trade(instrument, index, type='historical', parameters=params_to_modify)    
                else:
                    close_trade_df = self.__create_close_trade_df(index, open_trade, trade_info)
                    self.__tradeslist.store('close', instrument, close_trade_df)
                   
    def __close_trades_partial(self, instrument, trade_info):
        open_trades = self.__tradeslist.get_current_trades()[instrument]

        size_to_close = trade_info['position_size']

        for index, row in open_trades.iterrows():
            open_trade = open_trades.loc[index]
            open_trade_position_size = open_trade.position_size
            open_trade_direction = open_trade.direction

            if open_trade_direction == trade_info['direction']:

                if size_to_close >= open_trade_position_size:
                    self.__tradeslist.close(instrument, index, type='open')
                    close_trade_df = self.__create_close_trade_df(index, open_trade, trade_info)
                else: 
                    # Partial close of this open trade
                    close_trade_df = self.__create_close_trade_df(index, open_trade, trade_info, position_size=size_to_close)
                    params_to_modify = {
                        'position_size': open_trade_position_size - size_to_close
                    }
                    self.__tradeslist.modify_trade(instrument, index, type='current', parameters=params_to_modify)

                self.__tradeslist.store('close', instrument, close_trade_df)
                size_to_close -= open_trade_position_size

                if size_to_close <= 0: 
                    break         

    def __create_open_trade_df(self, trade_id, trade_info):
        position_size = trade_info['position_size']

        if (position_size > 0):
            direction = "long"
        elif (position_size < 0):
            direction = "short"
        else:
            raise ValueError("position_size cannot be zero")

        new_trade = {
            'timestamp': trade_info['timestamp'],
            'direction': direction,
            'entry_price': trade_info['open_price'],
            'position_size': abs(trade_info['position_size']),
            'stop_loss_price': trade_info['stop_loss_price'],
            'take_profit_price': trade_info['take_profit_price']
        }

        new_trade_df = pd.DataFrame(new_trade, index=[trade_id])

        return new_trade_df

    def __create_close_trade_df(self, trade_id, open_trade, trade_info, position_size=None):
        if position_size is None:
            position_size = open_trade.position_size

        closed_trade = {
            'time_open': open_trade.timestamp,
            'time_close': trade_info['timestamp'],
            'direction': open_trade.direction,
            'position_size': position_size,
            'entry_price': open_trade.entry_price,
            'stop_loss_price': open_trade.stop_loss_price,
            'take_profit_price': open_trade.take_profit_price,
            'exit_price': trade_info['exit_price']
        }

        closed_trade_df = pd.DataFrame(closed_trade, index=[trade_id])

        return closed_trade_df
    
    def __manage_open_trades(self, instrument, price_data):
        open_trades = self.__tradeslist.get_current_trades()

        # To handle the scenario where there are no open trades
        if instrument in open_trades:
            instrument_open_trades = open_trades[instrument]
        else:
            instrument_open_trades = pd.DataFrame()

        for index, row in instrument_open_trades.iterrows():
            open_trade = instrument_open_trades.loc[index]

            timestamp = price_data[0]['timestamp']
            stop_loss_price = open_trade.stop_loss_price
            take_profit_price = open_trade.take_profit_price

            # TODO: Handle when SL & TP is both within the range. Current Implementation will take the worst case scenario which takes into account hitting of stop loss

            sl_within_range = self.__is_price_within_range(stop_loss_price, price_data)            
            tp_witin_range = self.__is_price_within_range(take_profit_price, price_data)

            if sl_within_range:
                self.__close_managed_trade(instrument, index, open_trade, stop_loss_price, timestamp)
            elif tp_witin_range:
                self.__close_managed_trade(instrument, index, open_trade, take_profit_price, timestamp)

    def __manage_pending_orders(self, instrument, price_data):
        pending_orders = self.__tradeslist.get_pending_orders()

        # To handle the scenario where there are no pending position
        if instrument in pending_orders:
            instrument_pending_orders = pending_orders[instrument]
        else:
            instrument_pending_orders = pd.DataFrame()

        for index, row in instrument_pending_orders.iterrows():
            pending_order = instrument_pending_orders.loc[index]

            timestamp = price_data[0]['timestamp']
            entry_price = pending_order.entry_price

            entry_within_range = self.__is_price_within_range(entry_price, price_data)

            if entry_within_range:
                self.__open_managed_trade(instrument, index, pending_order, entry_price, timestamp)
        
    def __close_managed_trade(self, instrument, trade_id, trade, exit_price, timestamp):
        trade_info = {
            'timestamp': timestamp,
            'exit_price': exit_price,
            'position_size': None
        }

        closed_trade_df = self.__create_close_trade_df(trade_id, trade, trade_info)
        self.__tradeslist.close(instrument, trade_id, type='open')
        self.__tradeslist.store('close', instrument, closed_trade_df)

    def __open_managed_trade(self, instrument, order_id, order, entry_price, timestamp):
        trade_info = {
            'timestamp': timestamp,
            'open_price': entry_price,
            'position_size': order.position_size,
            'stop_loss_price': order.stop_loss_price,
            'take_profit_price': order.take_profit_price
        }

        open_trade_df = self.__create_open_trade_df(order_id, trade_info)
        self.__tradeslist.close(instrument, order_id, type='order')
        self.__tradeslist.store('open', instrument, open_trade_df)

    def __is_price_within_range(self, price_to_check, price_data):
        current_open = price_data[0]['open']    
        previous_high = price_data[1]['price']['high']
        previous_low = price_data[1]['price']['low']

        high = max(current_open, previous_high)
        low = min(current_open, previous_low)
            
        if price_to_check is not None and price_to_check <= high and price_to_check >= low:
            return True
        else:
            return False

    def open_trades(self):
        return self.__tradeslist.get_current_trades()

    def closed_trades(self):
        return self.__tradeslist.get_historical_trades()
