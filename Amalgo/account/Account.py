from .backtest.Backtest import Backtest
from Amalgo.tradeslist import TradesList

"""
    Handles the different account types

    Args:
        type: The account type.
        key: The API key for the account type.
        accounts: (Required for type oanda) The accounts for execution of trades.

    Returns:
        The different account type
"""


def Account(type, key=None, accounts=[]):
    types = {
        'backtest': __typeBackTest()
    }

    return types[type.lower()]


def __typeBackTest():
    tradeslist = TradesList(type='backtest')

    return Backtest(tradeslist)
