from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements_dev.txt", "r") as fh:
    requirements = fh.read()

setup(
    name="Amalgo",
    version="0.0.1",
    author="Amlago",
    author_email="author@example.com",
    description="Backtest and Live Trade Execution Package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    license='BSD',
    keywords=['amalgo'],
    packages=find_packages(exclude=['docs', 'tests', 'design', 'examples']),
    install_requires=requirements,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha'
        'Programming Language :: Python :: 3',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
    ],
)
