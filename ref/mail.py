def sendmail(mail_contents, password, smtp_server, port=25, alt_fromaddr=None):
    from smtplib import SMTP
    
    if alt_fromaddr == None:
        user = mail_contents['From']
    else:
        user = alt_fromaddr
    toaddr = mail_contents['To'].split(', ')
    
    server = SMTP(smtp_server, port)
    server.starttls()
    server.login(user, password)
    text = mail_contents.as_string()
    server.sendmail(user, toaddr, text)
    server.quit()
    print('Email sent')
    
def msg(fromaddr, toaddr_list, subject, body, attachment=None):
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.base import MIMEBase
    from email import encoders
    
    message = MIMEMultipart()
    message['From'] = fromaddr
    message['To'] = ', '.join(toaddr_list)
    message['Subject'] = subject
    message.attach(MIMEText(body, 'plain'))
    
    if attachment != None:
        filename = attachment.split('/')[-1]
        atchfile = open(attachment, 'rb').read()
        
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(atchfile)
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
        message.attach(part)
        
    return message

