#+--------------------------------------+
#| Indicators                           |
#+--------------------------------------+

# Outputs Donchian Prices
def DonchianH(high, period):
    return high.rolling(window=period,center=False).max()
def DonchianL(low, period):
    return low.rolling(window=period,center=False).min()

# MACD
def MACDHist(series, *MACDparam):
    macd = series.ewm(span=MACDparam[0], adjust=False).mean() - \
           series.ewm(span=MACDparam[1], adjust=False).mean()
    macd = macd - macd.ewm(span=MACDparam[2], adjust=False).mean()
    return macd

# Moving Average Crossover
def MACross(series, Fast, Slow):
    return series.rolling(window=Fast, center=False).mean() - \
           series.rolling(window=Slow, center=False).mean()

# Relative Strength Index
def RSI(series, period=14):
    import pandas as pd

    delta = series.diff()
    dUp, dDown = delta.copy(), delta.copy()
    dUp[dUp < 0] = 0
    dDown[dDown > 0] = 0

    rollUp = dUp.rolling(period).mean()
    rollDown = dDown.rolling(period).mean().abs()

    rs = rollUp / rollDown
    rsi= 100.0 - (100.0 / (1.0 + rs))
    return rsi

# Average True Range
def ATR(high, low, close, period):
    from pandas import concat
    highLow = high-low
    highClose = abs(high-close.shift(1))
    lowClose = abs(low-close.shift(1))
    TR = concat([highLow, highClose, lowClose], axis=1).max(axis=1)
    res = TR.rolling(period).mean()
    return res
