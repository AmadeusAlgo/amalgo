
#+--------------------------------------+
#| Library Setup                        |
#+--------------------------------------+

import numpy as np
from numpy.lib.stride_tricks import as_strided
import pandas as pd

#+--------------------------------------+
#| From maxdd.py                        |
#+--------------------------------------+

def windowed_view(x, window_size):
    """Creat a 2d windowed view of a 1d array.

    `x` must be a 1d numpy array.

    `numpy.lib.stride_tricks.as_strided` is used to create the view.
    The data is not copied.

    Example:

    >>> x = np.array([1, 2, 3, 4, 5, 6])
    >>> windowed_view(x, 3)
    array([[1, 2, 3],
           [2, 3, 4],
           [3, 4, 5],
           [4, 5, 6]])
    """
    y = as_strided(x, shape=(x.size - window_size + 1, window_size),
                   strides=(x.strides[0], x.strides[0]))
    return y


def rolling_max_dd(x, window_size, min_periods=1):
    """Compute the rolling maximum drawdown of `x`.

    `x` must be a 1d numpy array.
    `min_periods` should satisfy `1 <= min_periods <= window_size`.

    Returns an 1d array with length `len(x) - min_periods + 1`.
    """
    if min_periods < window_size:
        pad = np.empty(window_size - min_periods)
        pad.fill(x[0])
        x = np.concatenate((pad, x))
    y = windowed_view(x, window_size)
    running_max_y = np.maximum.accumulate(y, axis=1)
    dd = y - running_max_y
    return dd.min(axis=1)


def max_dd(ser):
    max2here = ser.expanding(1).max()
    dd2here = ser - max2here
    return -dd2here.min()

#+--------------------------------------+
#| A More Accurate Max DD               |
#+--------------------------------------+

def altmaxdd(strategy, initialBalance=1):

    df3 = pd.DataFrame(strategy)
    df3['Peak'] = strategy.cummax()
    df3['diff'] = df3.Peak - strategy
    df3['Drawdown'] = df3['diff'].cummax()
    return df3[df3['diff'] == df3.Drawdown].Drawdown.iloc[-1]/(df3[df3['diff'] == df3.Drawdown].Peak.iloc[-1] + initialBalance)

def Drawdown(strategy):

    df3 = pd.DataFrame(strategy)
    df3['Peak'] = strategy.cummax()
    df3['diff'] = df3.Peak - strategy
    return df3['diff'].cummax()

#+--------------------------------------+
#| Consecutive Wins & Losses            |
#+--------------------------------------+

def ConWin(data):
    df5 = pd.DataFrame(data)

    df5['Win'] = np.where(data > 0, 1, 0)
    df5['ConWin'] = df5.Win.cumsum()
    df5['ConWin2'] = np.where(df5.ConWin == df5.ConWin.shift(1),
                              df5.ConWin, None)
    df5['ConWin2'].fillna(method='ffill', inplace=True)
    df5['ConWin2'].fillna(value=0, inplace=True)
    df5['ConWin'] = df5.ConWin - df5.ConWin2

    return df5['ConWin']

def ConLoss(data):
    df5 = pd.DataFrame(data)

    df5['Loss'] = np.where(data < 0, 1, 0)
    df5['ConLoss'] = df5.Loss.cumsum()
    df5['ConLoss2'] = np.where(df5.ConLoss == df5.ConLoss.shift(1),
                               df5.ConLoss, None)
    df5['ConLoss2'].fillna(method='ffill', inplace=True)
    df5['ConLoss2'].fillna(value=0, inplace=True)
    df5['ConLoss'] = df5.ConLoss - df5.ConLoss2

    return df5['ConLoss']


def ConWin(data):
    df5 = pd.DataFrame(data)

    df5['Win'] = np.where(data > 0, 1, 0)
    df5['ConWin'] = df5.Win.cumsum()
    df5['ConWin'] = np.where((df5.ConWin > df5.ConWin.shift(1)), df5.ConWin, 0)
    df5['ConWin2'] = np.where((df5.ConWin > df5.ConWin.shift(1)) & (df5.ConWin.shift(1) != 0), None, 0)
    df5['ConWin2'] = np.where(((df5.ConWin > df5.ConWin.shift(1)) & (df5.ConWin.shift(1) == 0)) | \
                              (df5.ConWin == 1), df5.ConWin-1, df5.ConWin2)
    df5['ConWin2'].fillna(method='ffill', inplace=True)
    df5['ConWin'] = df5.ConWin - df5.ConWin2

    return df5['ConWin']

#+--------------------------------------+
#| MAE & MFE                            |
#+--------------------------------------+

# For MAE & MFE, we assume that they are used on long positions.
# For short positions, simply change the functions - use MAE for MFE calculations and vice versa.

def MAE(low, openPrice, openRegion, closeTime):
    df = pd.concat([low, openRegion, closeTime], axis=1)
    df['MAE'] = df.low
    df['MAE'] = df.groupby([openRegion.name]).MAE.transform(min)
    df['MAE'] = df.sort_index(ascending=False).groupby([closeTime.name]).MAE.cummin()
    df['MAE'] = openPrice - df.MAE
    return df['MAE']

def MFE(high, openPrice, openRegion, closeTime):
    df = pd.concat([high, openRegion, closeTime], axis=1)
    df['MFE'] = df.high
    df['MFE'] = df.groupby([openRegion.name]).MFE.transform(max)
    df['MFE'] = df.sort_index(ascending=False).groupby([closeTime.name]).MFE.cummax()
    df['MFE'] = openPrice - df.MFE
    return df['MAE']

